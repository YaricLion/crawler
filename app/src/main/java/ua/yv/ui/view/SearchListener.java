package ua.yv.ui.view;

import ua.yv.crawler.core.error.CrawlError;
import ua.yv.crawler.core.model.SearchResult;

public interface SearchListener {

    void onStart(String uri);
    void onSuccess(String uri, SearchResult result);
    void onError(String uri, CrawlError crawlError);
}
