package ua.yv.ui.view;

import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.yv.crawler.R;
import ua.yv.ui.model.result.SearchResponse;
import ua.yv.ui.model.result.Status;
import ua.yv.ui.view.list.MonitoringView;

public final class CrawlerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Map<String, Pair<Status, SearchResponse>> monitoring = new HashMap<>();
    private List<String> urls = new ArrayList<>();

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MonitoringView view = (MonitoringView) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_monitoring_item, parent, false);
        MonitoringViewHolder holder = new MonitoringViewHolder(view);
        return holder;
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Pair<Status, SearchResponse> monitor = monitoring.get(urls.get(position));
        ((MonitoringViewHolder) holder).bind(position + 1, monitor);
    }

    @Override public int getItemCount() {
        return monitoring.size();
    }

    public void addMonitoring(String uri, Pair<Status, SearchResponse> pair){
        monitoring.put(uri, pair);
        urls.add(uri);
        notifyDataSetChanged();
    }
    public void clearMonitoring(){
        monitoring.clear();
        urls.clear();
        notifyDataSetChanged();
    }

    public static final class MonitoringViewHolder extends RecyclerView.ViewHolder{

        private MonitoringView itemView;

        public MonitoringViewHolder(MonitoringView itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        public void bind(int index, Pair<Status, SearchResponse> pair) {
            itemView.bind(index, pair.first, pair.second);
        }
    }

}