package ua.yv.ui.view;

public interface Bindable<VM> {
    void bind(VM vm);
}
