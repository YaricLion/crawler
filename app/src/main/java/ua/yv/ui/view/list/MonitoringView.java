package ua.yv.ui.view.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ua.yv.crawler.R;
import ua.yv.crawler.utils.Exceptions;
import ua.yv.ui.model.result.SearchResponse;
import ua.yv.ui.model.result.Status;

public class MonitoringView extends FrameLayout {

    private static final int MAX_ITEMS = 10;

    @Bind(R.id.monitoring_url)          TextView urlView;
    @Bind(R.id.monitoring_index)        TextView indexView;
    @Bind(R.id.monitoring_status)       TextView statusView;

    @Bind(R.id.monitoring_found_error)  TextView errorView;
    @Bind(R.id.monitoring_found_count)  TextView countView;

    public MonitoringView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void bind(int index, Status status, SearchResponse response) {

        boolean found = (status == Status.FOUND);
        boolean error = (status == Status.ERROR);

        countView.setVisibility(found ? VISIBLE : INVISIBLE);
        errorView.setVisibility(error ? VISIBLE : INVISIBLE);

        urlView.setText(response.getUri());

        String statusInfo = fromStatus(getContext(),status);
        if(found) statusInfo = statusInfo + ":" + fromResponse(response);
        statusView.setText(statusInfo);

        if(found)
            countView.setText(String.valueOf(response.getSearchResult().getCount()));
        else if(error)
            errorView.setText(Exceptions.info(response.getCrawlError().getCause()));

        indexView.setText(String.valueOf(index));
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    private static String fromStatus(Context context, Status status){

        int resourceId = R.string.view_monitoring_loading;;

        switch (status){
            case FOUND:
                resourceId = R.string.view_monitoring_found;
                break;
            case NOT_FOUND:
                resourceId = R.string.view_monitoring_not_found;
                break;
            case ERROR:
                resourceId = R.string.view_monitoring_error;
                break;
        }

        return context.getString(resourceId);
    }

    private static String fromResponse(SearchResponse response){

        List<Integer> indices = response.getSearchResult().getIndices();
        int size = indices.size();
        int len = Math.min(size, MAX_ITEMS);

        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for(Integer index : indices){
            sb.append(index);
            if( ++ i  >= len)
                break;
            sb.append(',');
        }
        if(i < size)
            sb.append("...");
        sb.append(']');

        return sb.toString();
    }

}
