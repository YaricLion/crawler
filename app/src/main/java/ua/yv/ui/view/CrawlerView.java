package ua.yv.ui.view;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.commons.lang.math.NumberUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import ua.yv.crawler.R;
import ua.yv.ui.utils.Device;

public class CrawlerView extends RelativeLayout {

    @Bind(R.id.crawler_result_list) RecyclerView recyclerView;

    @Bind(R.id.crawler_url)       EditText urlView;
    @Bind(R.id.crawler_query)     EditText queryView;
    @Bind(R.id.crawler_threads)   EditText threadsCountView;
    @Bind(R.id.crawler_links)     EditText urlsCountView;

    @Bind(R.id.crawler_start_btn) Button startBtn;
    @Bind(R.id.crawler_stop_btn)  Button stopBtn;
    @Bind(R.id.crawler_pause_resume_btn) Button pauseResumeBtn;

    private CrawlerAdapter adapter;
    private Device device;

    private boolean pause = true;

    public interface OnCrawlerStateChangedListener {
        void onStarted();
        void onPaused();
        void onResumed();
        void onStopped();
    }

    private OnCrawlerStateChangedListener stateChangedListener;

    private boolean idle = true;

    public CrawlerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        adapter = new CrawlerAdapter();
        device = new Device(context);
    }


    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), getDisplayColumns(device), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        pauseResumeBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                togglePauseResume();
            }
        });

        startBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                start();
            }
        });

        stopBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                stop();
            }
        });

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                idle = ( newState == RecyclerView.SCROLL_STATE_IDLE );
            }
        });

        urlsCountView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    start();
                }
                return false;
            }
        });
    }

    public void start(){

        if(stateChangedListener != null) {

            pauseResumeBtn.setEnabled(true);
            pauseResumeBtn.setText(R.string.view_crawler_pause);
            pause = true;

            stopBtn.setEnabled(true);

            stateChangedListener.onStarted();
        }

    }

    public void stop(){

        if(stateChangedListener != null) {

            startBtn.setEnabled(true);
            stopBtn.setEnabled(false);

            pauseResumeBtn.setEnabled(false);
            pauseResumeBtn.setText(R.string.view_crawler_pause);
            pause = true;

            stateChangedListener.onStopped();
        }

    }

    public void togglePauseResume(){
        if(stateChangedListener != null) {
            if (pause) {
                pauseResumeBtn.setText(R.string.view_crawler_resume);
                stateChangedListener.onPaused();
            } else {
                pauseResumeBtn.setText(R.string.view_crawler_pause);
                stateChangedListener.onResumed();
            }
            pause = !pause;
        }
    }

    public void scrollToBottomIfIdle(){
        if(idle)
            getRecyclerView().scrollToPosition(getAdapter().getItemCount() - 1);
    }

    public CrawlerAdapter getAdapter() {
        return adapter;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setUrl(String url) {
        urlView.setText(url);
    }

    public void setQuery(String query) {
        queryView.setText(query);
    }

    public void setMaxUrls(int maxUrls) {
        urlsCountView.setText(String.valueOf(maxUrls));
    }

    public void setMaxThreads(int maxThreads) {
        threadsCountView.setText(String.valueOf(maxThreads));
    }

    public void setOnCrawlerStateChangedLister (OnCrawlerStateChangedListener lister) {
        this.stateChangedListener = lister;
    }

    public String getUrl(){
        return urlView.getText().toString();
    }

    public int getThreadsCount(){
        String value = threadsCountView.getText().toString();
        return NumberUtils.isDigits(value) ? Integer.parseInt(value) : 0;
    }

    public int getUrlsCount(){
        String value = urlsCountView.getText().toString();
        return NumberUtils.isDigits(value) ? Integer.parseInt(value) : 0;
    }

    public String getQuery(){
        return queryView.getText().toString();
    }

/*

    private RecyclerView.Adapter animate(RecyclerView.Adapter adapter){

        SlideInBottomAnimationAdapter wrappingAdapter = new SlideInBottomAnimationAdapter(adapter);
        wrappingAdapter.setFirstOnly(true);
        wrappingAdapter.setDuration(ANIMATION_DURATION);
        return  wrappingAdapter;
    }

*/

    public static int getDisplayColumns(Device device){

        boolean isTablet = device.isTablet();
        int orientation = device.orientation();
        boolean portrait = ( orientation == Configuration.ORIENTATION_PORTRAIT ) ;
        if(isTablet)
            return portrait ? 2 : 3;
        else
            return portrait ? 1 : 2;
    }

    private static String STATE_SUPER_CLASS = "SuperClass";
    private static String STATE_PAUSE_RESUME  = "PauseResume";
    private static String STATE_START_ENABLED = "StartEnabled";
    private static String STATE_PAUSE_RESUME_ENABLED = "PauseEnabled";
    private static String STATE_STOP_ENABLED  = "StopEnabled";

    @Override protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_SUPER_CLASS, super.onSaveInstanceState());
        bundle.putBoolean(STATE_PAUSE_RESUME, pause);
        bundle.putBoolean(STATE_START_ENABLED, startBtn.isEnabled());
        bundle.putBoolean(STATE_PAUSE_RESUME_ENABLED, pauseResumeBtn.isEnabled());
        bundle.putBoolean(STATE_STOP_ENABLED, stopBtn.isEnabled());

        return bundle;
    }

    @Override protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle)state;
            super.onRestoreInstanceState(bundle.getParcelable(STATE_SUPER_CLASS));

            pause = bundle.getBoolean(STATE_PAUSE_RESUME);
            pauseResumeBtn.setText(pause ? R.string.view_crawler_pause : R.string.view_crawler_resume);

            startBtn.setEnabled(bundle.getBoolean(STATE_START_ENABLED));
            pauseResumeBtn.setEnabled(bundle.getBoolean(STATE_PAUSE_RESUME_ENABLED));
            stopBtn.setEnabled(bundle.getBoolean(STATE_STOP_ENABLED));
        }
        else super.onRestoreInstanceState(state);
    }


}
