package ua.yv.ui.model.result;

public enum Status {
    LOADING, FOUND, NOT_FOUND, ERROR;
}
