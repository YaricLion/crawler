package ua.yv.ui.model.result;

import ua.yv.crawler.core.error.CrawlError;
import ua.yv.crawler.core.model.SearchResult;

public final class SearchResponse {

    private final String uri;
    private final SearchResult searchResult;
    private final CrawlError crawlError;

    private SearchResponse(String uri, SearchResult searchResult, CrawlError crawlError) {
        this.uri = uri;
        this.searchResult = searchResult;
        this.crawlError = crawlError;
    }

    public SearchResponse(String uri, SearchResult searchResult) {
        this(uri, searchResult, null);
    }

    public SearchResponse(String uri, CrawlError crawlError) {
        this(uri, null, crawlError);
    }

    public boolean isSuccessful(){
        return crawlError == null;
    }

    public CrawlError getCrawlError() {
        return crawlError;
    }

    public String getUri() {
        return uri;
    }

    public SearchResult getSearchResult() {
        return searchResult;
    }

}
