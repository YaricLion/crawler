package ua.yv.ui.activity;

import android.support.v4.util.Pair;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import ua.yv.crawler.Crawler;
import ua.yv.crawler.CrawlerConfig;
import ua.yv.crawler.core.error.CrawlError;
import ua.yv.crawler.core.listener.CrawlerPageListener;
import ua.yv.crawler.core.model.Page;
import ua.yv.crawler.core.model.SearchResult;
import ua.yv.ui.model.result.SearchResponse;
import ua.yv.ui.model.result.Status;

//Simple Presenter for Crawler activity
public final class CrawlerPresenter {

    private static final String TAG = CrawlerPresenter.class.getSimpleName();

    private CrawlerActivity activity;
    private Crawler crawler;

    private Map<String, Pair<Status, SearchResponse>> monitoring = new HashMap<>();

    public CrawlerPresenter(){
    }

    public void onActivityCreate(CrawlerActivity activity){
        this.activity = activity;
        Log.d(TAG, "onActivityCreate : " + monitoring.keySet().toString());
        publishAll();
    }

    public void onActivityDestroy(){
        if(!activity.isChangingConfigurations()) {
            destroyCrawler();
        }
        this.activity = null;
    }

    public void startCrawlJob(String url, String query, int maxThreads, int maxUrls){

        Log.d(TAG, "startCrawlJob");
        activity.clearCrawl();
        destroyCrawler();
        createCrawler(maxThreads, maxUrls);
        startCrawler(url, query);

    }

    private void createCrawler(int maxThreads, int maxUrls){
        Log.d(TAG, "createCrawler");
        crawler = Crawler.newInstance(createNewConfig(maxThreads, maxUrls));
    }

    private void startCrawler(String url, String query){
        Log.d(TAG, "startCrawler");
        monitoring.clear();
        crawler.enqueue(url, query, crawlerPageListener);
        crawler.crawl();
    }

    private void destroyCrawler(){
        Log.d(TAG, "destroyCrawler");
        if(crawler != null) {
            Log.d(TAG, "destroyCrawler not null");
            crawler.destroy();
            crawler = null;
        }
    }

    public void stopCrawlJob(){
        Log.d(TAG, "stopCrawlJob");
        destroyCrawler();
    }

    public void pauseCrawlJob(){
        Log.d(TAG, "pauseCrawlJob");
        if(crawler != null){
            Log.d(TAG, "pauseCrawlJob not null");
            crawler.pause();
        }
    }

    public void resumeCrawlJob(){
        Log.d(TAG, "resumeCrawlJob");
        if(crawler != null) {
            Log.d(TAG, "resumeCrawlJob not null");
            crawler.resumeIfPaused();
        }
    }

    private void publishAll(){
        if(activity != null){
            for( Map.Entry<String, Pair<Status, SearchResponse>> entry : monitoring.entrySet()){
                publish(entry.getKey(), entry.getValue());
            }
        }
    }

    private void save(String uri, Pair<Status, SearchResponse> loading) {
        monitoring.put(uri, loading);
    }

    private void publish(String uri, Pair<Status, SearchResponse> loading){
        if(activity != null){
            activity.onNext(uri, loading);
        }
    }

    private CrawlerPageListener crawlerPageListener = new CrawlerPageListener() {

        @Override public void onPageStarted(String uri) {

            Pair<Status, SearchResponse> loading = new Pair<>(Status.LOADING, new SearchResponse(uri, (SearchResult) null));
            save(uri, loading);
            publish(uri, loading);

        }

        @Override public void onPageCancelled(String uri) {

        }

        @Override public void onPageFailed(String uri, CrawlError crawlError) {

            SearchResponse response = new SearchResponse(uri, crawlError);
            Pair<Status, SearchResponse> error = new Pair<>(Status.ERROR, response);

            save(uri, error);
            publish(uri, error);
        }

        @Override public void onPageComplete(String uri, Page loadedPage, SearchResult searchResult) {

            Status status = searchResult.getCount() > 0 ? Status.FOUND : Status.NOT_FOUND;
            SearchResponse response = new SearchResponse(uri, searchResult);
            Pair<Status, SearchResponse> foundOrNot = new Pair<>(status, response);

            save(uri, foundOrNot);
            publish(uri, foundOrNot);
        }
    };

    private static CrawlerConfig createNewConfig(int maxThreads, int maxUrls){

        return new CrawlerConfig.Builder()
                .disableHttpCache()
                .threadPoolSize(maxThreads)
                .maxCrawlUriCount(maxUrls)
                .writeDebugLogs()
                .build();
    }

}
