package ua.yv.ui.activity;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.apache.commons.lang.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import ua.yv.crawler.R;
import ua.yv.ui.model.result.SearchResponse;
import ua.yv.ui.model.result.Status;
import ua.yv.ui.view.CrawlerAdapter;
import ua.yv.ui.view.CrawlerView;

public class CrawlerActivity extends AppCompatActivity {

    private static final String HTTP_PROT_PREFIX    = "http://";
    private static final String HTTP_S_PROT_PREFIX  = "https://";
    private static final String FTP_PROT_PREFIX     = "ftp://";

    private static final String DEF_URL = "https://www.oracle.com/java/index.html";
    private static final String DEF_QUERY = "Java";

    private static final int DEF_THREAD_POOL_SIZE = 4;
    private static final int DEF_MAX_URLS_COUNT   = 100;

    @Bind(R.id.crawler_view) CrawlerView crawlerView;

    private CrawlerPresenter crawlerPresenter;

    @Override public void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_crawler);

        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState == null)
            initWithDefaults();

        crawlerView.setOnCrawlerStateChangedLister(stateChangedListener);

        Object nonConfigInstance = getLastCustomNonConfigurationInstance();
        crawlerPresenter = nonConfigInstance != null ? (CrawlerPresenter)nonConfigInstance : new CrawlerPresenter();

        crawlerPresenter.onActivityCreate(this);

    }

    //One way to save presenter across configuration changes
    @Override public final Object onRetainCustomNonConfigurationInstance() {
        if(isChangingConfigurations() && crawlerPresenter != null)
            return crawlerPresenter;
        return super.onRetainCustomNonConfigurationInstance();
    }

    private CrawlerView.OnCrawlerStateChangedListener stateChangedListener = new CrawlerView.OnCrawlerStateChangedListener() {
        @Override public void onStarted() {

            if(!correctInput())
                return;

            String url = crawlerView.getUrl();
            String query = crawlerView.getQuery();
            int maxThreads = crawlerView.getThreadsCount();
            int maxUrls = crawlerView.getUrlsCount();

            if(!url.contains(HTTP_PROT_PREFIX)
                    && !url.contains(HTTP_S_PROT_PREFIX)
                    && !url.contains(FTP_PROT_PREFIX))

                url = HTTP_PROT_PREFIX + url;

            crawlerPresenter.startCrawlJob(url, query, maxThreads, maxUrls);
            Toast.makeText(getApplicationContext(), R.string.view_crawler_start, Toast.LENGTH_SHORT).show();
        }

        @Override public void onPaused() {
            crawlerPresenter.pauseCrawlJob();
            Toast.makeText(getApplicationContext(), R.string.view_crawler_pause, Toast.LENGTH_SHORT).show();
        }

        @Override public void onResumed() {
            crawlerPresenter.resumeCrawlJob();
            Toast.makeText(getApplicationContext() , R.string.view_crawler_resume, Toast.LENGTH_SHORT).show();
        }

        @Override public void onStopped() {
            crawlerPresenter.stopCrawlJob();
            Toast.makeText(getApplicationContext(), R.string.view_crawler_stop, Toast.LENGTH_SHORT).show();
        }
    };

    @Override  protected void onDestroy() {
        super.onDestroy();
        crawlerPresenter.onActivityDestroy();
    }

    private void initWithDefaults(){
        crawlerView.setUrl(DEF_URL);
        crawlerView.setQuery(DEF_QUERY);
        crawlerView.setMaxUrls(DEF_MAX_URLS_COUNT);
        crawlerView.setMaxThreads(DEF_THREAD_POOL_SIZE);
    }

    public void onNext(String uri, Pair<Status, SearchResponse> responsePair){

        CrawlerAdapter adapter = crawlerView.getAdapter();
        adapter.addMonitoring(uri, responsePair);
        crawlerView.scrollToBottomIfIdle();
    }

    public void clearCrawl(){
        CrawlerAdapter adapter = crawlerView.getAdapter();
        adapter.clearMonitoring();
    }



    private boolean correctInput(){

        String url = crawlerView.getUrl();

        if(StringUtils.isEmpty(url)){
            Toast.makeText(getApplicationContext(), R.string.act_crawler_empty_url, Toast.LENGTH_SHORT).show();
            return false;
        }

        String query = crawlerView.getQuery();

        if(StringUtils.isEmpty(query)){
            Toast.makeText(getApplicationContext(), R.string.act_crawler_empty_query, Toast.LENGTH_SHORT).show();
            return false;
        }

        int maxThreads = crawlerView.getThreadsCount();

        if(maxThreads  <= 0){
            Toast.makeText(getApplicationContext(), R.string.act_crawler_no_threads, Toast.LENGTH_SHORT).show();
            return false;
        }

        int maxUrls = crawlerView.getUrlsCount();

        if(maxUrls  <= 0){
            Toast.makeText(getApplicationContext(), R.string.act_crawler_no_urls, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
