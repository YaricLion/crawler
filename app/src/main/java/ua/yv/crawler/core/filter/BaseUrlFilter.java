package ua.yv.crawler.core.filter;

/**
 * Created by yariclion on 30.06.16.
 */
public class BaseUrlFilter implements UrlFilter {

    //public static final String URL_FILTER_REG_EX = "/^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$/";
    //public static final String URL_FILTER_REG_EX = "^((http[s]?|ftp):\\/)?\\/?([^:\\/\\s]+)((\\/\\w+)*\\/)([\\w\\-\\.]+[^#?\\s]+)(.*)?(#[\\w\\-]+)?$";
    public static final String URL_FILTER_REG_EX ="^((http[s]?|ftp):)(\\/\\/)([^:\\/\\s]+)((\\/\\w+)*\\/)([\\w\\-\\.]+[^#?\\s]+)(.*)?(#[\\w\\-]+)?$";

    @Override public boolean include(String url) {
        if(url == null || !url.matches(URL_FILTER_REG_EX))
            return false;
        return true;
    }

}
