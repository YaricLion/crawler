package ua.yv.crawler.core.parse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ua.yv.crawler.core.download.PageResponse;
import ua.yv.crawler.core.model.Page;
import ua.yv.crawler.utils.LOG;

public class JsoupParser implements PageParser{

    private static final String A_HREF = "a[href]";
    private static final String HREF   = "href";

    @Override public Page parse(PageResponse response, String baseUrl) throws PageParseException {
        try {

            InputStream bodyStream = response.getStream();
            String charsetName = response.getCharset();
            Document document = Jsoup.parse(bodyStream, charsetName, baseUrl);
            List<String> links = copyLinks(document.select(A_HREF), baseUrl);
            String text = document.body().text();

            return new Page(baseUrl, links, text);

        }catch (IOException e){
            LOG.e(e);
            throw new PageParseException(String.format("Error when trying to parse %s", baseUrl), e);
        }

    }

    private static List<String> copyLinks(Elements elements, String baseUri){

        List<String> links = null;
        if(!elements.isEmpty()) {
            links = new ArrayList<>(elements.size());
            for (Element element : elements) {
                String href = element.attr(HREF);
                links.add(href);
                /*if (href != null) {
                    if(PageDownloader.Scheme.belongs(href)) {
                        links.add(href);
                    }else {
                        Uri base = Uri.parse(baseUri);
                        PageDownloader.Scheme scheme = PageDownloader.Scheme.ofUri(baseUri);
                        String wrapped = scheme.wrap(base.getHost(), href);
                        if(wrapped.matches(URL_FILTER_REG_EX)) {
                            links.add(wrapped);
                        }
                    }
                }*/
            }
        }
        return links;
    }
}
