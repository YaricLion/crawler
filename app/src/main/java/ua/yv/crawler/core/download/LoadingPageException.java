package ua.yv.crawler.core.download;

import java.io.IOException;

/**
 * Created by yariclion on 30.06.16.
 */
public class LoadingPageException extends IOException {

    private final int code;
    private final String contentType;

    public LoadingPageException(String msg, int code, String contentType) {
        this(msg, null, code, contentType);
    }

    public LoadingPageException(String msg, Throwable cause, int code, String contentType) {
        super(msg, cause);
        this.code = code;
        this.contentType = contentType;
    }

    public int getCode() {
        return code;
    }

    public String getContentType() {
        return contentType;
    }

}
