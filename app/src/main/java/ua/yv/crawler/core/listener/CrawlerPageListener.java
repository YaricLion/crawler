package ua.yv.crawler.core.listener;

import ua.yv.crawler.core.error.CrawlError;
import ua.yv.crawler.core.model.Page;
import ua.yv.crawler.core.model.SearchResult;

public interface CrawlerPageListener {

	void onPageStarted(String uri);
	void onPageCancelled(String uri);
	void onPageFailed(String uri, CrawlError crawlError);
	void onPageComplete(String uri, Page loadedPage, SearchResult searchResult);

	CrawlerPageListener STUB = new CrawlerPageListener() {
		@Override public void onPageStarted(String uri) {}
		@Override public void onPageCancelled(String uri) {	}
		@Override public void onPageFailed(String uri, CrawlError crawlError) {	}
		@Override public void onPageComplete(String uri, Page loadedPage, SearchResult searchResult) {}
	};
}
