package ua.yv.crawler.core.download;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import ua.yv.crawler.core.filter.ContentTypeFilter;
import ua.yv.crawler.utils.Charsets;

public final class OkHttpDownloader implements PageDownloader {

    private final static String FAIL_TO_LOAD_EXC = " HTTP : [%s]; Content-Type : [%s]";
    private final static String CONTENT_TYPE = "Content-Type";

    private final static String UTF_8 = "UTF-8";
    private final static int HTTP_OK = 200;

    private final OkHttpClient client;
    private final ContentTypeFilter filter;

    //private final Map<String, Set<Call>> callMap = new HashMap<>();

    public OkHttpDownloader(OkHttpClient client, ContentTypeFilter filter) {
        this.client = client;
        this.filter = filter;
    }

    @Override public PageResponse load(String url) throws IOException {

        Request request = new Request.Builder().url(url).build();
        Call call = client.newCall(request);

        //saveCall(call, url);

        Response response = call.execute();
        int responseCode = response.code();
      /*  String responseSource = response.networkResponse() != null
                ? ("(network: " + response.networkResponse().code() + " over " + response.protocol() + ")")
                : "(cache)";*/
        /*LOG.d("%03d: %s %s%n", responseCode, url, responseSource);*/

        String contentType = response.header(CONTENT_TYPE);
        ResponseBody body = response.body();

        if (responseCode != HTTP_OK || !filter.filter(contentType)) {
            body.close();
            throw new LoadingPageException(String.format(FAIL_TO_LOAD_EXC, responseCode, contentType), responseCode, contentType);
        }

        String charset = Charsets.getCharsetFromContentType(contentType);
        if(charset == null) charset = UTF_8; //reasonable try

        InputStream inputStream = body.byteStream();
        int contentLength = (int) body.contentLength();

        return new PageResponse(inputStream, contentLength, charset);

    }
/*

    private void saveCall(Call call, String url){

        synchronized (callMap){

            Set<Call> callSet = callMap.get(url);
            if(callSet == null){
                callSet = new HashSet<Call>();
            }
            callSet.add(call);

        }
    }
*/

    public void cancel(String url){

       /* synchronized (callMap){
            Set<Call> callSet = callMap.get(url);
            if(callSet != null && !callSet.isEmpty()){
                for(Call call : callSet){
                    if(call.isExecuted())
                        call.cancel();
                }
                callMap.put(url, null);
            }
        }*/

    }


    public void shutdown(){
/*
        synchronized (callMap){

            for(Map.Entry<String, Set<Call>> entry : callMap.entrySet()){
                Set<Call> calls = entry.getValue();
                for(Call call : calls){
                    if(call.isExecuted())
                        call.cancel();
                }
            }
            callMap.clear();
        }*/
    }
}