package ua.yv.crawler.core.search;

import ua.yv.crawler.core.model.Page;
import ua.yv.crawler.core.model.SearchResult;
import com.javacodegeeks.stringsearch.BM;

import java.util.List;

/*https://en.wikipedia.org/wiki/String_searching_algorithm*/
/*https://www.javacodegeeks.com/2010/09/string-performance-exact-string.html*/
public class SimpleBMSearch implements Searcher {

    //Should be enough for test purpose
    @Override public SearchResult search(String query, Page page) {
        BM bm = BM.compile(query);
        List<Integer> idx = bm.findAll(page.getBody());
        return new SearchResult(query, idx);
    }

}
