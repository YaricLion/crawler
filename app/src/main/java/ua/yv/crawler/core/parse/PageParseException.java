package ua.yv.crawler.core.parse;

import java.io.IOException;

/**
 * Created by yariclion on 30.06.16.
 */
public class PageParseException extends IOException {
    public PageParseException(String msg) {
        super(msg);
    }

    public PageParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
