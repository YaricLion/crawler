package ua.yv.crawler.core.parse;

import ua.yv.crawler.core.download.PageResponse;
import ua.yv.crawler.core.model.Page;

public interface PageParser {
    Page parse(PageResponse response, String baseUrl) throws PageParseException;
}
