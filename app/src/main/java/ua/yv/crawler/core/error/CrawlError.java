
package ua.yv.crawler.core.error;

public class CrawlError {

	private final CrawlErrorType type;

	private final Throwable cause;

	public CrawlError(CrawlErrorType type, Throwable cause) {
		this.type = type;
		this.cause = cause;
	}

	public CrawlErrorType getType() {
		return type;
	}

	public Throwable getCause() {
		return cause;
	}

	public enum CrawlErrorType {
		IO_ERROR,
		PARSING_ERROR,
		UNKNOWN
	}
}