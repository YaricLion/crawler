package ua.yv.crawler.core.filter;

public class BaseContentTypeFilter implements ContentTypeFilter {

    //https://tools.ietf.org/html/rfc7231#section-3.1.1.1
    @Override public boolean filter(String contentType) {
        return contentType != null && contentType.contains("text");
    }

}
