package ua.yv.crawler.core.download;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import ua.yv.crawler.core.listener.CrawlerPageProgressListener;

public interface PageDownloader {

	//TODO Refactor with Response {InputStream, contentLength, charset}
	PageResponse load(String pageUri) throws IOException;
	void cancel(String pageUri);
	void shutdown();

	enum Scheme {

		HTTP("http"), HTTPS("https"), UNKNOWN("");

		private String scheme;
		private String uriPrefix;
		private String divider;

		Scheme(String scheme) {
			this.scheme = scheme;
			uriPrefix = scheme + "://";
			divider = "/";
		}

		public static Scheme ofUri(String uri) {
			if (uri != null) {
				for (Scheme s : values()) {
					if (s.belongsTo(uri)) {
						return s;
					}
				}
			}
			return UNKNOWN;
		}

		public boolean belongsTo(String uri) {
			return uri.toLowerCase(Locale.US).startsWith(uriPrefix);
		}

		public static boolean belongs(String uri){

			if(HTTP.belongsTo(uri))
				return true;
			if(HTTPS.belongsTo(uri))
				return true;

			return false;
		}

		public String wrap(String path) {
			return uriPrefix + path;
		}

		public String wrap(String host, String path){
			return uriPrefix + host + divider + path;
		}

		public String crop(String uri) {
			if (!belongsTo(uri)) {
				throw new IllegalArgumentException(String.format("URI [%1$s] doesn't have expected scheme [%2$s]", uri, scheme));
			}
			return uri.substring(uriPrefix.length());
		}
	}
}
