package ua.yv.crawler.core.filter;

/**
 * Created by yariclion on 30.06.16.
 */
public interface ContentTypeFilter {
    boolean filter(String contentType);
}
