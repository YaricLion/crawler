package ua.yv.crawler.core.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class SearchResult{

    private final String searchQuery;
    private final List<Integer> indices;
    private final Integer count;

    public SearchResult(String searchQuery, Integer[] indices) {
        this.searchQuery = searchQuery;
        this.indices = indices != null ? Arrays.asList(indices) : new ArrayList<Integer>();
        this.count = indices.length;
    }
    public SearchResult(String searchQuery, List<Integer> indices) {
        this.searchQuery = searchQuery;
        this.indices = indices != null ? new ArrayList<>(indices) : new ArrayList<Integer>();
        this.count = indices.size();
    }

    public Integer getCount() {
        return count;
    }

    //Defensive copy to be immutable
    public List<Integer> getIndices() {
        return new ArrayList<>(indices);
    }

    public String getSearchQuery() {
        return searchQuery;
    }

}
