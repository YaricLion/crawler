package ua.yv.crawler.core.filter;

public interface LinkWrapper {
    String wrap(String base, String link);
}
