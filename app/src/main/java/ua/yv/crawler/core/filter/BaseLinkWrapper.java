package ua.yv.crawler.core.filter;

import ua.yv.crawler.utils.LOG;

public class BaseLinkWrapper implements LinkWrapper {

    public String wrap(String base, String link){

        if(link.matches(BaseUrlFilter.URL_FILTER_REG_EX)) {
            return link;
        }else {
            String wrapped;
            if(base.endsWith("/"))
                base = base.substring(0, base.length() - 1);
            if(link.startsWith("/"))
                link = link.substring(1, link.length());
            wrapped = base + "/" + link;

            if(wrapped.matches(BaseUrlFilter.URL_FILTER_REG_EX)) {
                return wrapped;
            }
        }

        return null;
    }

}
