package ua.yv.crawler.core.download;

import java.io.InputStream;

public final class PageResponse {

    final InputStream stream;
    final long contentLength;
    final String charset;

    public PageResponse(InputStream stream, long contentLength, String charset) {
        this.stream = stream;
        this.contentLength = contentLength;
        this.charset = charset;
    }

    public InputStream getStream() {
        return stream;
    }

    public long getContentLength() {
        return contentLength;
    }

    public String getCharset() {
        return charset;
    }
}
