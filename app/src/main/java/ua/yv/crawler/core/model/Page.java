package ua.yv.crawler.core.model;

import java.util.ArrayList;
import java.util.List;

public final class Page {

    private final String url;
    private final List<String> links;
    private final String body;

    public Page(String url, List<String> links, String body) {
        this.url = url;
        this.links = links != null ? new ArrayList<>(links) : new ArrayList<String>();
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public String getBody() {
        return body;
    }

    public List<String> getLinks() {
        return new ArrayList<>(links);
    }
}
