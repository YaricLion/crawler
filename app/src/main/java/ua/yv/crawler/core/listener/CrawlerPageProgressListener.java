package ua.yv.crawler.core.listener;

public interface CrawlerPageProgressListener {

	void onProgressUpdate(String uri, int current, int total);

	CrawlerPageProgressListener STUB = new CrawlerPageProgressListener() {
		@Override public void onProgressUpdate(String uri, int current, int total) {
		}
	};
}
