package ua.yv.crawler.core.search;

import ua.yv.crawler.core.model.Page;
import ua.yv.crawler.core.model.SearchResult;

public interface Searcher {
    SearchResult search(String quesry, Page page);
}
