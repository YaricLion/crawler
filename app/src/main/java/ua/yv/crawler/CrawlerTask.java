package ua.yv.crawler;

import android.os.Handler;


import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import ua.yv.crawler.core.error.CrawlError;
import ua.yv.crawler.core.download.PageDownloader;
import ua.yv.crawler.core.download.PageResponse;
import ua.yv.crawler.core.filter.LinkWrapper;
import ua.yv.crawler.core.filter.UrlFilter;
import ua.yv.crawler.core.listener.CrawlerPageListener;
import ua.yv.crawler.core.listener.CrawlerPageProgressListener;
import ua.yv.crawler.core.model.Page;
import ua.yv.crawler.core.model.SearchResult;
import ua.yv.crawler.core.parse.PageParseException;
import ua.yv.crawler.core.parse.PageParser;
import ua.yv.crawler.core.search.Searcher;
import ua.yv.crawler.utils.IO;
import ua.yv.crawler.utils.LOG;

final class CrawlerTask implements Runnable {

	private static final String LOG_WAITING_FOR_RESUME = "Crawler is paused. Waiting...  [%s]";
	private static final String LOG_RESUME_AFTER_PAUSE = ".. Resume loading [%s]";
	private static final String LOG_LOAD_PAGE_FROM_NETWORK = "Load page from network [%s]";
	private static final String LOG_TASK_INTERRUPTED = "Task was interrupted [%s]";

	private static final String LOG_TIMES = "CRAWL TIME : Loading took [%s], parsing - [%s] from [%s] ";
	private static final String LOG_SEARCH_TIME = "CRAWL TIME : Searching took [%s], from [%s] ";

	private static final String ERROR_NO_PAGE = "No page [%s]";

	private final CrawlerEngine engine;
	private final CrawlerTaskInfo crawlerTaskInfo;
	private final Handler handler;

	private final CrawlerConfig configuration;
	private final PageDownloader downloader;
	private final PageParser pageParser;
	private final Searcher searcher;
	private final UrlFilter urlFilter;
	private final LinkWrapper linkWrapper;

	final String uri;
	final String query;

	final CrawlerPageListener listener;
	final CrawlerPageProgressListener progressListener;

/*	final ConcurrentHashMap<String, AtomicInteger> hostNames;*/
	final AtomicInteger visitCounter;

	final int maxHostVisitCount;
	final int maxUriCount;

	public CrawlerTask(CrawlerEngine engine, CrawlerTaskInfo crawlerTaskInfo, Handler handler) {

		this.engine = engine;
		this.crawlerTaskInfo = crawlerTaskInfo;
		this.handler = handler;

		configuration = engine.configuration;
		downloader = configuration.downloader;
		pageParser = configuration.parser;
		searcher = configuration.searcher;
		urlFilter = configuration.urlFilter;
		linkWrapper = configuration.linkWrapper;

		/*hostNames = engine.hostNames;*/
		visitCounter = engine.visitCounter;

		maxHostVisitCount = configuration.maxHostVisitCount;
		maxUriCount = configuration.maxUriCount;

		uri = crawlerTaskInfo.uri;
		query = crawlerTaskInfo.query;
		listener = crawlerTaskInfo.listener;
		progressListener = crawlerTaskInfo.progressListener;
	}

	@Override public void run() {

		waitIfPaused();

		//Skip all if we exceeded the limit
		if (visitCounter.getAndIncrement() >= maxUriCount) return;

		startNewPageEvent(uri);

		Page page;
		SearchResult searchResult;

		try {
			page = loadPage();
			if (page == null) return; // listener callback already was fired
			checkTaskInterrupted();

			long searchTime = System.currentTimeMillis();
			searchResult = searcher.search(query, page);
			searchTime = System.currentTimeMillis() - searchTime;
			LOG.d(LOG_SEARCH_TIME, searchTime, uri);

			completeEvent(page, searchResult);

			/*
			// Skip hosts that we've visited many times.
			AtomicInteger hostnameCount = new AtomicInteger();
			AtomicInteger previous = hostNames.putIfAbsent(url.getHost(), hostnameCount);
			if (previous != null) hostnameCount = previous;
			if (hostnameCount.incrementAndGet() >= maxHostVisitCount) return;*/

			List<String> links = page.getLinks();
			LOG.d("PAGE : " + uri + " - > links : " + links);
			for (String link : links) {

				checkTaskInterrupted();

				String wrappedUrl = linkWrapper.wrap(uri, link);
				LOG.d("PAGE " + link + " - > wrappedUrl : " + wrappedUrl);
				if(urlFilter.include(wrappedUrl)) {
					CrawlerTaskInfo crawlerTaskInfo = new CrawlerTaskInfo(wrappedUrl, query, listener, progressListener/*, engine.getLockForUri(link)*/);
					engine.submit(crawlerTaskInfo);
				}

			}

		}
		catch (TaskCancelledException e) {
			cancelEvent();
		} finally {

		}

	}

	private void waitIfPaused() {
		AtomicBoolean pause = engine.getPause();
		if (pause.get()) {
			synchronized (engine.getPauseLock()) {
				LOG.d(LOG_WAITING_FOR_RESUME, uri);
				while (pause.get()) {
					try {
						engine.getPauseLock().wait();
					} catch (InterruptedException e) {
						LOG.e(LOG_TASK_INTERRUPTED, uri);
					}
				}
				LOG.d(LOG_RESUME_AFTER_PAUSE, uri);
			}
		}
	}

	private Page loadPage() throws TaskCancelledException {
		Page page = null;
		try {
				LOG.d(LOG_LOAD_PAGE_FROM_NETWORK, uri);
				PageResponse response = null;
				try{

					long loadTime = System.currentTimeMillis();
					response = downloader.load(uri);
					loadTime = System.currentTimeMillis() - loadTime;

					if (response == null) {
						LOG.e(ERROR_NO_PAGE, uri);
						return null;
					}

					long parseTime = System.currentTimeMillis();
					page = pageParser.parse(response, uri);
					parseTime = System.currentTimeMillis() - parseTime;

					LOG.d(LOG_TIMES, loadTime, parseTime, uri);

					return page;
				} finally {
					if(response != null)
						IO.closeQuietly(response.getStream());
				}
		} catch (IllegalStateException e) {
		} catch (PageParseException e){
			LOG.e(e);
			failEvent(CrawlError.CrawlErrorType.PARSING_ERROR, e);
		}/*catch (TaskCancelledException e) {
			throw e;
		}*/	catch (IOException e) {
			LOG.e(e);
			failEvent(CrawlError.CrawlErrorType.IO_ERROR, e);
		} catch (Throwable e) {
			LOG.e(e);
			failEvent(CrawlError.CrawlErrorType.UNKNOWN, e);
		}
		return page;
	}

	/*
	@Override public boolean onBytesCopied(int current, int total) {
		return progressEvent(current, total);
	}
	*/

	private boolean progressEvent(final int current, final int total) {
		if (isTaskInterrupted() ) return false;
		if (progressListener != null) {
			Runnable r = new Runnable() {
				@Override public void run() {
					progressListener.onProgressUpdate(uri, current, total);
				}
			};
			runTask(r, handler);
		}
		return true;
	}

	private void failEvent(final CrawlError.CrawlErrorType crawlErrorType, final Throwable failCause) {
		if (isTaskInterrupted()) return;
		Runnable r = new Runnable() {
			@Override public void run() {
				listener.onPageFailed(uri, new CrawlError(crawlErrorType, failCause));
			}
		};
		runTask(r, handler);
	}

	private void cancelEvent() {
		if (isTaskInterrupted()) return;
		Runnable r = new Runnable() {
			@Override public void run() {
				listener.onPageCancelled(uri);
			}
		};
		runTask(r, handler);
	}

	private void completeEvent(final Page page, final SearchResult searchResult){
		if (isTaskInterrupted()) return;
		Runnable r = new Runnable() {
			@Override public void run() {
				listener.onPageComplete(uri, page, searchResult);
			}
		};
		runTask(r, handler);
	}

	private void startNewPageEvent(final String link){
		if (isTaskInterrupted()) return;
		Runnable r = new Runnable() {
			@Override public void run() {
				listener.onPageStarted(link);
			}
		};
		runTask(r, handler);
	}

	private void checkTaskInterrupted() throws TaskCancelledException {
		if (isTaskInterrupted()) {
			throw new TaskCancelledException();
		}
	}

	private boolean isTaskInterrupted() {
		if (Thread.interrupted()) {
			LOG.d(LOG_TASK_INTERRUPTED, uri);
			return true;
		}
		return false;
	}

	static void runTask(Runnable r, Handler handler) {
		if(handler != null && r != null)
			handler.post(r);
	}

	class TaskCancelledException extends Exception {
	}
}
