
package ua.yv.crawler;

import android.text.TextUtils;

import ua.yv.crawler.core.listener.CrawlerPageListener;
import ua.yv.crawler.core.listener.CrawlerPageProgressListener;
import ua.yv.crawler.utils.LOG;

public final class Crawler {

	static final String LOG_INIT_CONFIG = "Initialize Crawler with configuration";
	static final String LOG_DESTROY = "STATE: Destroy Crawler";
	static final String LOG_START   = "STATE: Start Crawler";
	static final String LOG_STOP    = "STATE: Stop Crawler";
	static final String LOG_RESUME  = "STATE: Resume Crawler";
	static final String LOG_PAUSE   = "STATE: Pause Crawler";

	private static final String WARNING_RE_INIT_CONFIG = "Try to initialize Crawler which had already been initialized before. " + "To re-init Crawler with new configuration call Crawler.destroy() at first.";
	private static final String ERROR_NOT_INIT = "Crawler must be init with configuration before using";
	private static final String ERROR_INIT_CONFIG_WITH_NULL = "Crawler configuration can not be initialized with null";

	public static final String TAG = Crawler.class.getSimpleName();

	private CrawlerConfig configuration;
	private CrawlerEngine engine;

	private final CrawlerPageListener defaultListener = CrawlerPageListener.STUB;
	private final CrawlerPageProgressListener defaultProgressListener = CrawlerPageProgressListener.STUB;

	private Crawler() {
	}

	public static Crawler newInstance(CrawlerConfig configuration) {
		Crawler crawler = new Crawler();
		crawler.init(configuration);
		return crawler;
	}

	private void init(CrawlerConfig configuration) {
		if (configuration == null) {
			throw new IllegalArgumentException(ERROR_INIT_CONFIG_WITH_NULL);
		}
		if (this.configuration == null) {
			LOG.d(LOG_INIT_CONFIG);
			engine = new CrawlerEngine(configuration);
			this.configuration = configuration;
		} else {
			LOG.w(WARNING_RE_INIT_CONFIG);
		}
	}

	public boolean isInit() {
		return configuration != null;
	}

	public void enqueue(String uri, String query){
		enqueue(uri, query, null);
	}

	public void enqueue(String uri, String query, CrawlerPageListener listener){
		enqueue(uri, query, listener, null);
	}

	public void enqueue(String uri, String query, CrawlerPageListener listener, CrawlerPageProgressListener progressListener){

		checkConfiguration();

		if (TextUtils.isEmpty(uri))
			return;

		if (listener == null)
			listener = defaultListener;

		if(progressListener == null)
			progressListener = defaultProgressListener;

		CrawlerTaskInfo crawlerTaskInfo = new CrawlerTaskInfo(uri, query, listener, progressListener /*, engine.getLockForUri(uri)*/);
		engine.enqueue(crawlerTaskInfo);

	}

	public void crawl(){
		engine.crawl();
		LOG.d(LOG_START);
	}

	private void checkConfiguration() {
		if (configuration == null) {
			throw new IllegalStateException(ERROR_NOT_INIT);
		}
	}

	public void pause() {
		engine.pause();
		LOG.d(LOG_PAUSE);
	}

	public boolean resumeIfPaused(){
		LOG.d(LOG_RESUME);
		return engine.resumeIfPaused();
	}

	public void resume() {
		engine.resume();
		LOG.d(LOG_RESUME);
	}

	public void stop() {
		engine.stop();
		LOG.d(LOG_STOP);
	}

	public void destroy() {
		if (configuration != null) LOG.d(LOG_DESTROY);
		stop();
		engine = null;
		configuration = null;
	}

}
