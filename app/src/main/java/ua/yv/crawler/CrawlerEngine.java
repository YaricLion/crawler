package ua.yv.crawler;

import android.os.Handler;
import android.os.Looper;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import ua.yv.crawler.core.download.PageDownloader;

class CrawlerEngine {

	final CrawlerConfig configuration;

	private final PageDownloader pageDownloader;

	private final Object executorLock = new Object();
	private Executor taskExecutor;
/*
	private final Map<String, ReentrantLock> uriLocks = new WeakHashMap<String, ReentrantLock>();*/

	private final AtomicBoolean paused = new AtomicBoolean(false);
	private final Object pauseLock = new Object();

	final int maxUriCount;
	final int maxHostVisitCount;

	/* Queue for crawling Pages Graph */
	//final LinkedBlockingQueue<CrawlerTaskInfo> queue = new LinkedBlockingQueue<>();
	//final HashSet<String> seedsUri = new HashSet<>();
	//final LinkedList<CrawlerTaskInfo> seeds = new LinkedList<CrawlerTaskInfo>();

	private String seedUri;
	private CrawlerTaskInfo seed;

	/* Already fetched urls */
	final Set<String> fetchedUrls = new LinkedHashSet<>();
	/* How much times host is visited */
	//final ConcurrentHashMap<String, AtomicInteger> hostNames = new ConcurrentHashMap<>();
	/*Urls Visit Counter */
	final AtomicInteger visitCounter = new AtomicInteger(0);

	//TODO should be customized in CrawlerConfig to run not only on thr main thread
	private final Handler callHandler = new Handler();

	CrawlerEngine(CrawlerConfig configuration) {
		this.configuration = configuration;
		pageDownloader = configuration.downloader;
		taskExecutor = configuration.taskExecutor;

		maxUriCount = configuration.maxUriCount;
		maxHostVisitCount = configuration.maxHostVisitCount;
	}

	void enqueue(CrawlerTaskInfo info){
		checkSameThread();
		/*if(seedsUri.add(info.uri)) { //filter same uris
			seeds.push(info);
		}*/
		this.seed = info;
		this.seedUri = info.uri;
	}

	void crawl() {

		checkSameThread();

		//initExecutorIfNeed();

		/*int initSeedsSize = seeds.size();    //Could be couple seeds in the beginning
		if (initSeedsSize == 0)
			LOG.d(LOG_EMPTY_INIT_QUEUE_STATE);

		CrawlerTaskInfo crawlerTaskInfo;
		while ((crawlerTaskInfo = seeds.poll()) != null) {

			//CrawlerTask crawlerTask = new CrawlerTask(this, crawlerTaskInfo, callHandler);
			//submit(crawlerTask);
			submit(crawlerTaskInfo);
		}*/
		synchronized (fetchedUrls) {
			if (fetchedUrls.isEmpty() && !fetchedUrls.contains(seedUri)) {
				submit(seed);
				seed = null;
			}
		}
	}

	/** Submits task to execution pool */
	private void submit(final CrawlerTask task) {
		synchronized (executorLock) {
			//initExecutorIfNeed();
			if(!((ExecutorService) taskExecutor).isShutdown())
				taskExecutor.execute(task);
		}
	}

	/** Submits task to execution pool */
	void submit(final CrawlerTaskInfo crawlerTaskInfo) {
		synchronized (fetchedUrls) {
			if (fetchedUrls.add(crawlerTaskInfo.uri)) { //filter already visited urls
				CrawlerTask crawlerTask = new CrawlerTask(this, crawlerTaskInfo, callHandler);
				submit(crawlerTask);
			}
		}
	}


	private void initExecutorIfNeed() {
		synchronized (executorLock) {
			if (((ExecutorService) taskExecutor).isShutdown()) {
				taskExecutor = createTaskExecutor();
			}
		}
	}

	private Executor createTaskExecutor() {
		return CrawlerConfigFactory.createExecutor(configuration.threadPoolSize, configuration.threadPriority);
	}

	void pause() {
		checkSameThread();
		paused.set(true);
	}

	boolean resumeIfPaused(){
		checkSameThread();

		boolean resumed = paused.compareAndSet(true, false);
		synchronized (pauseLock) {
			if(resumed) {
				pauseLock.notifyAll();
			}
		}
		return resumed;
	}

	void resume() {
		checkSameThread();
		paused.set(false);
		synchronized (pauseLock) {
			pauseLock.notifyAll();
		}
	}

	void stop() {
		checkSameThread();
		pageDownloader.shutdown();
		synchronized (executorLock) {
			((ExecutorService) taskExecutor).shutdownNow();
		}
		cleanQueue();
	}

	void cleanQueue(){
		checkSameThread();
		synchronized (fetchedUrls) {
			fetchedUrls.clear();
		}
	}

	AtomicBoolean getPause() {
		return paused;
	}

	Object getPauseLock() {
		return pauseLock;
	}

	public void checkSameThread() {
		if (Looper.myLooper() != callHandler.getLooper()) {
			throw new IllegalStateException("Must be invoked from the same thread.");
		}
	}

}
