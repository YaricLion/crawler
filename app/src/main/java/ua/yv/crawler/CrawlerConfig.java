package ua.yv.crawler;

import java.io.File;
import java.util.concurrent.Executor;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import ua.yv.crawler.core.download.PageDownloader;
import ua.yv.crawler.core.filter.ContentTypeFilter;
import ua.yv.crawler.core.filter.LinkWrapper;
import ua.yv.crawler.core.filter.UrlFilter;
import ua.yv.crawler.core.parse.PageParser;
import ua.yv.crawler.core.search.Searcher;
import ua.yv.crawler.utils.LOG;

public final class CrawlerConfig {

	final Executor taskExecutor;
	final boolean customExecutor;

	final int threadPoolSize;
	final int threadPriority;
	final int maxHostVisitCount;
	final int maxUriCount;

	final PageDownloader downloader;
	final PageParser parser;
	final Searcher searcher;
	final UrlFilter urlFilter;
	final LinkWrapper linkWrapper;
	final ContentTypeFilter contentTypeFilter;

	private CrawlerConfig(final Builder builder) {

		taskExecutor = builder.taskExecutor;
		threadPoolSize = builder.threadPoolSize;
		threadPriority = builder.threadPriority;
		maxHostVisitCount = builder.maxHostVisitCount;
		maxUriCount = builder.maxUriCount;
		downloader = builder.downloader;
		parser = builder.parser;
		searcher = builder.searcher;
		urlFilter = builder.urlFilter;
		linkWrapper = builder.linkWrapper;
		customExecutor = builder.customExecutor;
		contentTypeFilter = builder.contentTypeFilter;

		LOG.writeDebugLogs(builder.writeLogs);
	}

	public static CrawlerConfig createDefault() {
		return new Builder().build();
	}

	public static class Builder {


		public static final int DEFAULT_THREAD_POOL_SIZE = 3;
		public static final int DEFAULT_THREAD_PRIORITY = Thread.NORM_PRIORITY - 2;
		public static final int DEFAULT_MAX_HOST_VISIT_COUNT = 200;
		public static final int DEFAULT_MAX_CRAWL_URI_COUNT = 200;

		public static final long DEFAULT_HTTP_CACHE_BYTE_COUNT = 1024L * 1024L * 100L;

		private Executor taskExecutor = null;
		private boolean customExecutor = false;

		private int threadPoolSize = DEFAULT_THREAD_POOL_SIZE;
		private int threadPriority = DEFAULT_THREAD_PRIORITY;

		private int maxHostVisitCount = DEFAULT_MAX_HOST_VISIT_COUNT;
		private int maxUriCount = DEFAULT_MAX_CRAWL_URI_COUNT;

		private long httpCacheByteCount = DEFAULT_HTTP_CACHE_BYTE_COUNT;

		private boolean shouldUseHttpCache = false;

		private String httpCacheDir;

		private PageDownloader downloader;
		private PageParser parser;
		private Searcher searcher;

		private UrlFilter urlFilter;
		private LinkWrapper linkWrapper;

		private ContentTypeFilter contentTypeFilter;

		private boolean writeLogs = false;

		public Builder() {
		}

		public Builder taskExecutor(Executor executor) {
			this.taskExecutor = executor;
			return this;
		}

		public Builder threadPoolSize(int threadPoolSize) {
			this.threadPoolSize = threadPoolSize;
			return this;
		}

		public Builder maxCrawlUriCount(int maxCrawlUriCount) {
			this.maxUriCount = maxCrawlUriCount;
			return this;
		}

		public Builder maxHostVisitCount(int maxHostVisitCount) {
			this.maxHostVisitCount = maxHostVisitCount;
			return this;
		}

		public Builder pageDownloader(PageDownloader pageDownloader) {
			this.downloader = pageDownloader;
			return this;
		}

		public Builder pageParser(PageParser pageParser) {
			this.parser = pageParser;
			return this;
		}

		public Builder pageSearcher(Searcher searcher) {
			this.searcher = searcher;
			return this;
		}

		public Builder urlFilter(UrlFilter urlFilter) {
			this.urlFilter = urlFilter;
			return this;
		}

		public Builder linkWrapper(LinkWrapper linkWrapper) {
			this.linkWrapper = linkWrapper;
			return this;
		}

		public Builder contentTypeFilter(ContentTypeFilter contentTypeFilter) {
			this.contentTypeFilter = contentTypeFilter;
			return this;
		}

		public Builder writeDebugLogs() {
			this.writeLogs = true;
			return this;
		}

		public CrawlerConfig build() {
			init();
			return new CrawlerConfig(this);
		}

		public Builder disableHttpCache(){
			this.shouldUseHttpCache = false;
			return this;
		}

		public Builder enableHttpCache(){
			this.shouldUseHttpCache = true;
			return this;
		}

		public Builder httpCacheDir(String httpCacheDir){
			this.httpCacheDir = httpCacheDir;
			return this;
		}

		private void init() {
			if (taskExecutor == null) {
				taskExecutor = CrawlerConfigFactory.createExecutor(threadPoolSize, threadPriority);
			} else {
				customExecutor = true;
			}

			if(contentTypeFilter == null){
				contentTypeFilter = CrawlerConfigFactory.createContentTypeFilter();
			}

			if (downloader == null) {

				OkHttpClient client;
				OkHttpClient.Builder builder = new OkHttpClient.Builder();
				if(shouldUseHttpCache && httpCacheDir != null) {
					Cache cache = new Cache(new File(httpCacheDir), httpCacheByteCount);
					client = builder.cache(cache).build();
				}else {
					client = builder.build();
				}
				downloader = CrawlerConfigFactory.createPageDownloader(client, contentTypeFilter);
			}

			if (searcher == null) {
				searcher = CrawlerConfigFactory.createSearcher();
			}
			if (parser == null) {
				parser = CrawlerConfigFactory.createPageParser();
			}
			if(urlFilter == null){
				urlFilter = CrawlerConfigFactory.createUrlFilter();
			}
			if(linkWrapper == null){
				linkWrapper = CrawlerConfigFactory.createLinkWrapper();
			}

		}
	}

}
