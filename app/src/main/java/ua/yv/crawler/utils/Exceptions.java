package ua.yv.crawler.utils;

import android.os.Looper;

public final class Exceptions {

    private static final int MAX_MSG_LEN = 100;

    private Exceptions(){}

    public static String info(Throwable exception){

        String clazz = exception.getClass().getSimpleName();
        String msg = exception.getMessage();

        StringBuilder info = new StringBuilder();
        info.append(clazz);

        if(msg != null){
            info.append("::");
            if(msg.length() > MAX_MSG_LEN ) {
                info.append(msg.substring(0, MAX_MSG_LEN)).append("...");
            }else {
                info.append(msg);
            }
        }
        return info.toString();
    }


}
