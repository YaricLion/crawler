package ua.yv.crawler.utils;

import java.io.Closeable;

public final class IO {

	private IO() {
	}

	public static void closeQuietly(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (Exception ignored) {
			}
		}
	}

}
