package ua.yv.crawler;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import okhttp3.OkHttpClient;
import ua.yv.crawler.core.download.OkHttpDownloader;
import ua.yv.crawler.core.download.PageDownloader;
import ua.yv.crawler.core.filter.BaseContentTypeFilter;
import ua.yv.crawler.core.filter.BaseLinkWrapper;
import ua.yv.crawler.core.filter.BaseUrlFilter;
import ua.yv.crawler.core.filter.ContentTypeFilter;
import ua.yv.crawler.core.filter.LinkWrapper;
import ua.yv.crawler.core.filter.UrlFilter;
import ua.yv.crawler.core.parse.JsoupParser;
import ua.yv.crawler.core.parse.PageParser;
import ua.yv.crawler.core.search.SimpleBMSearch;
import ua.yv.crawler.core.search.Searcher;

public class CrawlerConfigFactory {

	public static Executor createExecutor(int threadPoolSize, int threadPriority) {
		return Executors.newFixedThreadPool(threadPoolSize, createThreadFactory(threadPriority, "crawler-pool-"));
	}

	public static PageDownloader createPageDownloader(OkHttpClient okHttpClient, ContentTypeFilter filter) {
		return new OkHttpDownloader(okHttpClient, filter);
	}

	public static PageParser createPageParser() {
		return new JsoupParser();
	}

	public static UrlFilter createUrlFilter() {
		return new BaseUrlFilter();
	}

	public static LinkWrapper createLinkWrapper() {
		return new BaseLinkWrapper();
	}

	public static ContentTypeFilter createContentTypeFilter() {
		return new BaseContentTypeFilter();
	}


	public static Searcher createSearcher() {
		return new SimpleBMSearch();
	}

	private static ThreadFactory createThreadFactory(int threadPriority, String threadNamePrefix) {
		return new CrawlerThreadFactory(threadPriority, threadNamePrefix);
	}

	private static class CrawlerThreadFactory implements ThreadFactory {

		private static final AtomicInteger poolNumber = new AtomicInteger(1);

		private final ThreadGroup group;
		private final AtomicInteger threadNumber = new AtomicInteger(1);
		private final String namePrefix;
		private final int threadPriority;

		CrawlerThreadFactory(int threadPriority, String threadNamePrefix) {
			this.threadPriority = threadPriority;
			group = Thread.currentThread().getThreadGroup();
			namePrefix = threadNamePrefix + poolNumber.getAndIncrement() + "-thread-";
		}

		@Override public Thread newThread(Runnable r) {
			Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
			if (t.isDaemon()) t.setDaemon(false);
			t.setPriority(threadPriority);
			return t;
		}
	}
}
