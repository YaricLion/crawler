package ua.yv.crawler;

import ua.yv.crawler.core.listener.CrawlerPageListener;
import ua.yv.crawler.core.listener.CrawlerPageProgressListener;

final class CrawlerTaskInfo {

	final String uri;
	final String query;
	final CrawlerPageListener listener;
	final CrawlerPageProgressListener progressListener;

	public CrawlerTaskInfo(String uri, String query, CrawlerPageListener listener,
						   CrawlerPageProgressListener progressListener) {
		this.uri = uri;
		this.query = query;
		this.listener = listener;
		this.progressListener = progressListener;
	}

}
